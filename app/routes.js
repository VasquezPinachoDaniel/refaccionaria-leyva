var Productos = require('./models/productos_model');
var Compras = require('./models/compras_model');
var Ventas = require('./models/ventas_model');
var Proveedores = require('./models/proveedores_model');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = function(app) {
  /********************************************/
  /***************URLs for Productos***************/
  app.get('/api/productos', function(req, res) { //Get all the productos in DB
    Productos.find(function(err, productos) {
      if (err) res.send(err);
      res.json(productos);
    }).sort('nombre');
  });
  app.get('/api/productos/:nombre', function(req, res) { //Get all the productos in DB
    Productos.find({ $text: {$search: req.params.nombre, $caseSensitive: false}
                  },function(err, productos) {
      if (err) res.send(err);
      res.json(productos);
    });
  });
  app.get('/api/productos-existencia', function(req, res) { //Get all the productos in DB
    Productos.find({existencia: {$lte: 10}},function(err, productos) {
      if (err) res.send(err);
      res.json(productos);
    }).sort('nombre');
  });
  app.get('/api/productos/:id', function(req, res) { //Obtiene un producto en DB
    Productos.findById({ _id: req.params.id }, function(err, producto) {
      if (err) res.send(err);
      res.json(producto);
    });
  });
  app.post('/api/productos', function(req, res) { //Create a new producto in DB
    producto = new Productos(req.body);
    producto.save(function(err) {
      if (err) res.send(err);
      res.json({code: 0, message: 'Éxito al insertar el nuevo producto.'});
    });
  });
  app.put('/api/productos', function(req, res) { //Update a producto in DB
    Productos.findByIdAndUpdate(req.body._id, req.body, function(err){
      if (err) res.send(err);
      res.json({code: 0, message: 'Éxito al actualizar el producto ' + req.body.nombre + '.'});
    });
  });
  app.put('/api/productos/actualizar-existencia', function(req, res) { //Incrementar existencia al realizar una compra
    Productos.findByIdAndUpdate(req.body._id, {$inc: {'existencia': parseInt(req.body.cantidad_comprada)}}, function(err){
      if (err) res.send(err);
      res.json({code: 0, message: 'Éxito al actualizar la existencia del producto ' + req.body.nombre});
    });
  });
  app.put('/api/productos/actualizar-existencia-v', function(req, res) { //Incrementar existencia al realizar una compra
    Productos.findByIdAndUpdate(req.body._id, {$inc: {'existencia': (parseInt(req.body.cantidad_vendida) * -1)}}, function(err){
      if (err) res.send(err);
      res.json({code: 0, message: 'Éxito al actualizar la existencia del producto'});
    });
  });
  app.delete('/api/productos', function(req, res) { //Delete a producto in DB
    Productos.findOneAndRemove(req.body, function(err){
      if (err) res.send(err);
      res.json({code: 0, message: 'Producto eliminado exitosamente.'});
    });
  });
  /***************END of URLs for Productos***************/
/***************************************************/
/********************************************/
/***************URLs for Compras***************/
  app.get('/api/compras/:fecha_inicial/:fecha_final', function(req, res) { //Obtener las compras dentro de un rango de fechas
    var fi = new Date(req.params.fecha_inicial), ff = new Date(req.params.fecha_final);
    Compras.aggregate([
      { $unwind: "$productos" },
      { $lookup: {
        from: "productos",
        localField: "productos._id",
        foreignField: "_id",
        as: 'producto_completo'
        }
      },
      { $lookup: {
        from: "proveedores",
        localField: "proveedor",
        foreignField: "_id",
        as: 'proveedor_completo'
        }
      },
      { $match: {
        fecha_facturacion: {$gte: fi, $lte: ff}
        }
      },
      { $sort: {
        fecha_facturacion: -1
        }
      },
      { $group: {
        _id: "$_id",
        clave_factura: {$first: "$clave_factura"},
        fecha_facturacion: {$first: "$fecha_facturacion"},
        fecha_pago: {$first: "$fecha_pago"},
        fecha_limite_pago: {$first: "$fecha_limite_pago"},
        estado: {$first: "$estado"},
        valor: {$first: "$valor"},
        comentario: {$first: "$comentario"},
        productos: {$push: "$productos"},
        proveedor_completo: {$first: "$proveedor_completo"},
        productos_completos: {$push: "$producto_completo"}
        }
      }
    ], function(err, compras) {
      if (err) res.send(err);
      res.json(compras);
    });
  });
  app.get('/api/compras-pagar/:hoy', function(req, res) { //Registrar pago de la compra
    var fi = new Date(req.params.hoy);
    fi.setDate(fi.getDate() + 1);
    var ff = fi;
    console.log('Fecha inicial: ' + fi);
    ff.setDate(fi.getDate() + 5);
    console.log('Fecha final: ' + ff);
    Compras.aggregate([
      { $unwind: "$productos" },
      { $lookup: {
        from: "productos",
        localField: "productos._id",
        foreignField: "_id",
        as: 'producto_completo'
        }
      },
      { $lookup: {
        from: "proveedores",
        localField: "proveedor",
        foreignField: "_id",
        as: 'proveedor_completo'
        }
      },
      { $match: {
        fecha_limite_pago: {$lte: ff},
        fecha_pago: {$exists: false}
        }
      },
      { $sort: {
        fecha_limite_pago: -1
        }
      },
      { $group: {
        _id: "$_id",
        clave_factura: {$first: "$clave_factura"},
        fecha_facturacion: {$first: "$fecha_facturacion"},
        fecha_pago: {$first: "$fecha_pago"},
        fecha_limite_pago: {$first: "$fecha_limite_pago"},
        estado: {$first: "$estado"},
        valor: {$first: "$valor"},
        comentario: {$first: "$comentario"},
        productos: {$first: "$productos"},
        proveedor_completo: {$first: "$proveedor_completo"},
        productos_completos: {$push: "$producto_completo"}
        }
      }
    ], function(err, compras) {
      if (err) res.send(err);
      res.json(compras)
    });
  });
  app.post('/api/compras', function(req, res) { //Guardar nueva Compra
    compra = new Compras(req.body);
    var ff = new Date(compra.fecha_facturacion), flp = new Date(compra.fecha_facturacion);
    if(ff.getMonth() == 12) {
      flp.setMonth(1);
      flp.setYear(ff.getYear() + 1);
    }
    else
      compra.fecha_limite_pago = flp.setMonth(ff.getMonth() + 1);
    compra.save(function(err) {
      if (err) res.send(err);
      res.json({code: 0, message: 'Éxito al insertar la nueva compra.'});
    });
  });
  app.put('/api/pago-compra/:id/:fecha_pago', function(req, res) { //Registrar pago de la compra
    Compras.findByIdAndUpdate(req.params.id, {'fecha_pago': req.params.fecha_pago}, function(err){
      if (err) res.send(err);
      res.json({code: 0, message: 'Éxito al registrar el pago de la compra'});
    });
  });
  app.put('/api/compras', function(req, res) { //Actualizar una compra en DB
    delete req.body.productos_completos;
    Compras.findByIdAndUpdate(req.body._id, req.body, function(err){
      if (err) res.send(err);
      res.json({code: 0, message: 'Éxito al actualizar la compra'});
    });
  });
  app.delete('/api/compras/:id', function(req, res) { //Elimina una compra en DB
    Compras.findByIdAndRemove(req.params.id, function(err){
      if (err) res.send(err);
      res.json({code: 0, message: 'Compra eliminada exitosamente.'});
    });
  });
/***************FIN de URLs para Compras***************/
/***************************************************/
/********************************************/
/***************URLs for Ventas***************/
  app.get('/api/ventas/:fecha_inicial/:fecha_final', function(req, res) { //Obtener las ventas dentro de un rango de fechas
    var fi = new Date(req.params.fecha_inicial), ff = new Date(req.params.fecha_final);
    Ventas.aggregate([
      { $unwind: "$productos" },
      { $lookup: {
        from: "productos",
        localField: "productos._id",
        foreignField: "_id",
        as: 'producto_completo'
        }
      },
      { $match: {
        fecha: {$gte: fi, $lte: ff}
        }
      },
      { $sort: {
        fecha: -1
        }
      },
      { $group: {
        _id: "$_id",
        cliente: {$first: "$cliente"},
        clave_factura: {$first: "$clave_factura"},
        fecha: {$first: "$fecha"},
        valor: {$first: "$valor"},
        comentario: {$first: "$comentario"},
        productos: {$push: "$productos"},
        productos_completos: {$push: "$producto_completo"}
        }
      }
    ], function(err, ventas) {
      if (err) res.send(err);
      res.json(ventas);
    });
  });
  app.post('/api/ventas', function(req, res) { //Guardar nueva Venta
    venta = new Ventas(req.body);
    venta.save(function(err) {
      if (err) res.send(err);
      res.json({code: 0, message: 'Éxito al insertar la nueva venta.'});
    });
  });
  app.put('/api/ventas', function(req, res) { //Actualizar una venta en DB
    delete req.body.productos_completos;
    Ventas.findByIdAndUpdate(req.body._id, req.body, function(err){
      if (err) res.send(err);
      res.json({code: 0, message: 'Éxito al actualizar la venta'});
    });
  });
  app.delete('/api/ventas/:id', function(req, res) { //Elimina una venta en DB
    Ventas.findByIdAndRemove(req.params.id, function(err){
      if (err) res.send(err);
      res.json({code: 0, message: 'Venta eliminada exitosamente.'});
    });
  });
/***************FIN de URLs para Ventas***************/
/***************************************************/
/********************************************/
/***************URLs para Proveedores***************/
  app.get('/api/proveedores', function(req, res) { //Obtener todos los proveedores
    Proveedores.find(function(err, proveedores) {
      if (err) res.send(err);
      res.json(proveedores);
    }).sort('nombre');
  });
  app.post('/api/proveedores', function(req, res) { //Crear un nuevo proveedor en la BD
    proveedor = new Proveedores(req.body);
    proveedor.save(function(err) {
      if (err) res.send(err);
      res.json({code: 0, message: 'Éxito al insertar el nuevo proveedor.'});
    });
  });
  app.put('/api/proveedores', function(req, res) { //Actualizar un proveedor en DB
    Proveedores.findByIdAndUpdate(req.body._id, req.body, function(err){
      if (err) res.send(err);
      res.json({code: 0, message: 'Éxito al actualizar el proveedor'});
    });
  });
  app.delete('/api/proveedores', function(req, res) { //Elimina un proveedor en DB
    Proveedores.findOneAndRemove(req.body, function(err){
      if (err) res.send(err);
      res.json({code: 0, message: 'Proveedor eliminada exitosamente.'});
    });
  });
/***************FIN de URLs para Proveedores***************/
/***************************************************/
  app.get('*', function(req, res) {
    res.sendfile('./public/index.html');
  });
};
