// grab the mongoose module
var mongoose = require('mongoose');

// module.exports allows us to pass this to other files when it is called
module.exports = mongoose.model('productos', {
    nombre : String,
    cantidad: String,
    numero_parte: String,
    descripcion: String,
    precio_compra: Number,
    precio_venta: Number,
    existencia: Number
});
