angular.module('refacApp').controller('ProveedoresCtrl', ['$scope', '$http', 'toaster',
  function($scope, $http, toaster) {
    $(document).ready(function(){
      //$('.modal').modal('show');
    });
    var showMessage = (function(mes) {
      toaster.pop({
              type: 'info',
              title: 'Success',
              body: mes
          });
    });
    $scope.nuevo_proveedor =  {
      nombre : 'Prueba proveedor',
      telefono: 9541201579,
      direccion: 'Dirección del proveedor',
      email: 'daniel.pinacho@gmail.com',
      rfc: 'RFCCPR0V33D0R',
      datos_bancarios: 'Cuenta de banco, CLABE de transferencia',
      comentario: 'Algún comentario extra'
    };
    var obtenerProveedores = (function() {
      var req = {
        method: 'GET',
        url: 'http://127.0.0.1:3000/api/proveedores',
        headers: {'Content-Type': 'application/json'},
      };
      $http(req).then(function(res){
        $scope.proveedores = res.data;
      }, function(res){});
    });
    $scope.guardarProveedor = (function() {
      var req = {
        method: 'POST',
        url: 'http://127.0.0.1:3000/api/proveedores',
        headers: {'Content-Type': 'application/json'},
        data: $scope.nuevo_proveedor
      };
      $http(req).then(function(res){
        obtenerProveedores();
        showMessage(res.data.message);
      }, function(res){});
    });
    $scope.editarProveedor = (function(proveedor) {
      proveedor.editMode = true;
      $scope.proveedor = proveedor;
    });
    $scope.actualizarProveedor = (function() {
      var req = {
        method: 'PUT',
        url: 'http://127.0.0.1:3000/api/proveedores',
        headers: {'Content-Type': 'application/json'},
        data: $scope.proveedor
      };
      $http(req).then(function(res){
        obtenerProveedores();
        $scope.proveedor.editMode = false;
        showMessage(res.data.message);
      }, function(res){});
    });
    $scope.eliminarProveedor = (function(proveedor) {
      if(confirm('Seguro que quieres eliminar el proveedor ' + proveedor.nombre + '?')) {
        var req = {
          method: 'DELETE',
          url: 'http://127.0.0.1:3000/api/proveedores',
          headers: {'Content-Type': 'application/json'},
          data: proveedor
        };
        $http(req).then(function(res){
          obtenerProveedores();
          showMessage(res.data.message);
        }, function(res){});
      }
    });
    obtenerProveedores();
  } //Fin del controlador
]);
