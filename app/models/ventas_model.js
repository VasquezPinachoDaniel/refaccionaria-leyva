// grab the mongoose module
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// module.exports allows us to pass this to other files when it is called
module.exports = mongoose.model('ventas', {
    productos : [{
      _id: Schema.ObjectId,
      cantidad_vendida: Number
    }],
    cliente: String,
    clave_factura: String,
    fecha: Date,
    valor: Number,
    comentario: String
});
