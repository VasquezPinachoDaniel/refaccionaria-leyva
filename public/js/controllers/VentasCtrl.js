angular.module('refacApp').controller('VentasCtrl', ['$scope', '$http', 'toaster',
  function($scope, $http, toaster) {
    $scope.fecha_inicial = '2016-12-01';
    $scope.fecha_final = '2016-12-31';
    $scope.productos_seleccionados = [];
    $scope.nueva_venta = {
      cliente: 'Público general',
      clave_factura: 'Público general',
      fecha: '2016-12-25',
      comentario: 'Comentarios para la venta'
    };
    $('.fechas input').datepicker({
      dateFormat: "yy-mm-dd"
    });
    var showMessage = (function(mes) {
      toaster.pop({
              type: 'info',
              title: 'Success',
              body: mes
          });
    });
    var obtenerProductos = (function() {
      var req = {
        method: 'GET',
        url: 'http://127.0.0.1:3000/api/productos',
        headers: {'Content-Type': 'application/json'},
      };
      $http(req).then(function(res){
        $scope.productos = res.data;
      }, function(res){});
    });
    $scope.guardarVenta = (function() {
      $scope.nueva_venta.productos = $scope.productos_seleccionados;
      var req = {
        method: 'POST',
        url: 'http://127.0.0.1:3000/api/ventas',
        headers: {'Content-Type': 'application/json'},
        data: $scope.nueva_venta
      };
      $http(req).then(function(res){
        $scope.consultarVentas();
        showMessage(res.data.message);
        for (var i = 0; i < $scope.productos_seleccionados.length; i++) {
          //Pertición para actualizar las existencias del producto
          var req = {
            method: 'PUT',
            url: 'http://127.0.0.1:3000/api/productos/actualizar-existencia-v',
            headers: {'Content-Type': 'application/json'},
            data: $scope.productos_seleccionados[i]
          };
          $http(req).then(function(res){
            showMessage(res.data.message + ' ' + $scope.productos_seleccionados[i].nombre);
          }, function(res){});
        }
      }, function(res){});
    });
    $scope.consultarVentas = (function() {
      var req = {
        method: 'GET',
        url: 'http://127.0.0.1:3000/api/ventas/' + $scope.fecha_inicial + '/' + $scope.fecha_final,
        headers: {'Content-Type': 'application/json'},
      };
      $http(req).then(function(res){
        for (var i = 0; i < res.data.length; i++)
          res.data[i].fecha = res.data[i].fecha.substr(0,10);
        $scope.ventas = res.data;
      }, function(res){});
    });
    $scope.editarVenta = (function(co) {
      co.editMode = true;
      $scope.venta = co;
    });
    $scope.actualizarVenta = (function() {
      var req = {
        method: 'PUT',
        url: 'http://127.0.0.1:3000/api/ventas',
        headers: {'Content-Type': 'application/json'},
        data: $scope.venta
      };
      $http(req).then(function(res){
        $scope.venta.editMode = false;
        showMessage(res.data.message);
      }, function(res){});
    });
    $scope.eliminarVenta = (function(venta) {
      if(confirm('¿Seguro que quieres eliminar esta venta?')) {
        var id = $scope.ventas.indexOf(venta);
        $scope.ventas.splice(id, 1);
        var req = {
          method: 'DELETE',
          url: 'http://127.0.0.1:3000/api/ventas/' + venta._id,
          headers: {'Content-Type': 'application/json'}
        };
        $http(req).then(function(res){
          showMessage(res.data.message);
        }, function(res){});
      }
    });
    $scope.seleccionarProducto = (function(pro) {
      if(pro.seleccionado)
        $scope.productos_seleccionados.push(pro);
      else {
        var index = $scope.productos_seleccionados.indexOf(pro);
        if(index > -1)
          $scope.productos_seleccionados.splice(index, 1);
      }
      $scope.actualizarValorVenta();
    });
    $scope.actualizarValorVenta = (function() {
      $scope.nueva_venta.valor = 0;
      console.log($scope.productos_seleccionados);
      if($scope.productos_seleccionados.length > 0) {
        for (var i = 0; i < $scope.productos_seleccionados.length; i++) {
          $scope.nueva_venta.valor = $scope.nueva_venta.valor +
            ($scope.productos_seleccionados[i].precio_venta * $scope.productos_seleccionados[i].cantidad_vendida);
        }
      }
    });
    obtenerProductos();
  } //Fin del controlador
]);
