// grab the mongoose module
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// module.exports allows us to pass this to other files when it is called
module.exports = mongoose.model('compras', {
    productos : [{
      producto: Schema.ObjectId,
      cantidad_comprada: Number
    }],
    clave_factura: String,
    fecha_facturacion: Date,
    fecha_pago: Date,
    estado: {type: String, default:'No pagada'},
    fecha_limite_pago: Date,
    proveedor: Schema.ObjectId,
    valor: Number,
    comentario: String
});
