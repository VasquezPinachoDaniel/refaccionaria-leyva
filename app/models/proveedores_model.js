// grab the mongoose module
var mongoose = require('mongoose');

// module.exports allows us to pass this to other files when it is called
module.exports = mongoose.model('proveedores', {
    nombre: String,
    telefono: Number,
    direccion: String,
    email: String,
    rfc: String,
    datos_bancarios: String,
    comentario: String
});
