var app = angular.module('refacApp', ['ngRoute', 'toaster', 'ngAnimate']);
app.config(function($routeProvider) {
  $routeProvider
  .when('/', {
    templateUrl: 'partials/home.html'
  })
  .when('/ventas', {
    templateUrl: 'partials/ventas.html'
  })
  .when('/compras', {
    templateUrl: 'partials/compras.html'
  })
  .when('/proveedores', {
    templateUrl: 'partials/proveedores.html'
  })
  .otherwise({redirectTo: '/'});
});

app.service('Notificaciones', ['$http', function($http) {
  this.obtenProductos = (function() {
    var req = {
      method: 'GET',
      url: 'http://127.0.0.1:3000/api/productos-existencia',
      headers: {'Content-Type': 'application/json'},
    };
    return $http(req);
  });
  this.obtenPagos = (function(hoy) {
    var req = {
      method: 'GET',
      url: 'http://127.0.0.1:3000/api/compras-pagar/' + hoy,
      headers: {'Content-Type': 'application/json'},
    };
    return $http(req);
  });
  this.prueba = function() {
    console.log('Impriendo desde el servicio');
  };
}]);
