angular.module('refacApp').controller('HomeCtrl', ['$scope', '$http', 'toaster', 'Notificaciones',
  function($scope, $http, toaster, Notificaciones) {
    var comprobarNotificaciones = (function() {
      var hoy = new Date(); hoy = hoy.toISOString(); hoy = hoy.substr(0,10);
      Notificaciones.obtenProductos().then(function(res) {
        $scope.productos_existencia = res.data;
        Notificaciones.obtenPagos(hoy).then(function(res) {
          for (var i = 0; i < res.data.length; i++) {
            res.data[i].fecha_limite_pago = res.data[i].fecha_limite_pago.substr(0,10);
            res.data[i].fecha_facturacion = res.data[i].fecha_facturacion.substr(0,10);
          }
          $scope.compras_pagar = res.data;
          if($scope.compras_pagar.length > 0 || $scope.productos_existencia.length > 0)
            $('#notificaciones').modal('show');
          else
            showMessage('No hay pendientes');
        });
      });
    });
    $(document).ready(function(){
      comprobarNotificaciones();
    });
    var showMessage = (function(mes) {
      toaster.pop({
              type: 'info',
              title: 'Notificación',
              body: mes
          });
    });
    $scope.nuevo_producto =  {
      nombre : 'Prueba producto',
      cantidad: 10,
      numero_parte: 'X',
      descripcion: 'Sin descripción - Producto',
      precio_compra: 250,
      existencia: 20
    };
    $scope.buscarProductosXNombre = (function() {
      if($scope.buscarProducto.length > 2) {
        var req = {
          method: 'GET',
          url: 'http://127.0.0.1:3000/api/productos/' + $scope.buscarProducto,
          headers: {'Content-Type': 'application/json'},
        };
        $http(req).then(function(res){
          if(res.data.length > 0) {
            var nxp = 15, div = Math.floor(res.data.length / nxp), rem = res.data.length % nxp;
            if(rem > 0)
              div++;
            var np = 1, ind = 0;
            $scope.paginas = [div];
            $scope.paginas[0] = [];
            for (var i = 0; i < res.data.length; i++) {
              if (np > nxp) {
                np = 1;
                ind++;
                $scope.paginas[ind] = [];
              }
              $scope.paginas[ind].push(res.data[i]);
              np++;
            }
            $scope.productos = $scope.paginas[0];
            $scope.indice = 0;
          }
        }, function(res){});
      } else
          $scope.productos = [];
    });
    $scope.cambioPagina = (function(ind) {
      if(ind > -1 && ind < $scope.paginas.length) {
        $scope.productos = $scope.paginas[ind];
        $scope.indice = ind;
      }
    });
    $scope.guardarProducto = (function() {
      $scope.nuevo_producto.precio_compra = $scope.nuevo_producto.precio_compra +
                                            ($scope.nuevo_producto.precio_compra * 0.16) +
                                            ($scope.nuevo_producto.precio_compra * 0.025);
      $scope.nuevo_producto.precio_venta = $scope.nuevo_producto.precio_compra * 1.2;
      var req = {
        method: 'POST',
        url: 'http://127.0.0.1:3000/api/productos',
        headers: {'Content-Type': 'application/json'},
        data: $scope.nuevo_producto
      };
      $http(req).then(function(res){
        buscarProductosXNombre();
        showMessage(res.data.message);
        comprobarNotificaciones();
      }, function(res){});
    });
    $scope.editarProducto = (function(producto) {
      producto.editMode = true;
      $scope.producto = producto;
    });
    $scope.actualizarProducto = (function() {
      var req = {
        method: 'PUT',
        url: 'http://127.0.0.1:3000/api/productos',
        headers: {'Content-Type': 'application/json'},
        data: $scope.producto
      };
      $http(req).then(function(res){
        buscarProductosXNombre();
        $scope.producto.editMode = false;
        showMessage(res.data.message);
        comprobarNotificaciones();
      }, function(res){});
    });
    $scope.eliminarProducto = (function(producto) {
      if(confirm('Seguro que quieres eliminar el producto ' + producto.nombre + '?')) {
        var req = {
          method: 'DELETE',
          url: 'http://127.0.0.1:3000/api/productos',
          headers: {'Content-Type': 'application/json'},
          data: producto
        };
        $http(req).then(function(res){
          buscarProductosXNombre();
          showMessage(res.data.message);
        }, function(res){});
      }
    });
  } //Fin del controlador
]);
