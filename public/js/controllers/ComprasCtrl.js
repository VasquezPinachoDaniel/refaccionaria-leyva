angular.module('refacApp').controller('ComprasCtrl', ['$scope', '$http', 'toaster',
  function($scope, $http, toaster) {
    $scope.fecha_inicial = '2016-11-01';
    $scope.fecha_final = '2016-12-31';
    $scope.productos_seleccionados = [];
    $scope.nueva_compra = {
      proveedor: 'Proveedor X',
      cantidad: 20,
      valor: 2400,
      comentario: 'Sin comentario'
    };
    $('.fechas input').datepicker({
      dateFormat: "yy-mm-dd"
    });
    var showMessage = (function(mes) {
      toaster.pop({
              type: 'info',
              title: 'Success',
              body: mes
          });
    });
    var obtenerProductos = (function() {
      var req = {
        method: 'GET',
        url: 'http://127.0.0.1:3000/api/productos',
        headers: {'Content-Type': 'application/json'},
      };
      $http(req).then(function(res){
        $scope.productos = res.data;
      }, function(res){});
    });
    var obtenerProveedores = (function() {
      var reqp = {
        method: 'GET',
        url: 'http://127.0.0.1:3000/api/proveedores',
        headers: {'Content-Type': 'application/json'},
      };
      $http(reqp).then(function(res){
        $scope.proveedores = res.data;
      }, function(res){});
    });
    $scope.guardarCompra = (function() {
      $scope.nueva_compra.proveedor = $scope.proveedor._id;
      $scope.nueva_compra.productos = $scope.productos_seleccionados;
      var req = {
        method: 'POST',
        url: 'http://127.0.0.1:3000/api/compras',
        headers: {'Content-Type': 'application/json'},
        data: $scope.nueva_compra
      };
      $http(req).then(function(res){
        $scope.consultarCompras();
        showMessage(res.data.message);
        for (var i = 0; i < $scope.productos_seleccionados.length; i++) {
          //Pertición para actualizar las existencias del producto
          var req = {
            method: 'PUT',
            url: 'http://127.0.0.1:3000/api/productos/actualizar-existencia',
            headers: {'Content-Type': 'application/json'},
            data: $scope.productos_seleccionados[i]
          };
          $http(req).then(function(res){
            showMessage(res.data.message);
          }, function(res){});
        }
      }, function(res){});
    });
    $scope.consultarCompras = (function() {
      var req = {
        method: 'GET',
        url: 'http://127.0.0.1:3000/api/compras/' + $scope.fecha_inicial + '/' + $scope.fecha_final,
        headers: {'Content-Type': 'application/json'},
      };
      $http(req).then(function(res){
        for (var i = 0; i < res.data.length; i++) {
          res.data[i].fecha_facturacion = res.data[i].fecha_facturacion.substr(0,10);
          res.data[i].fecha_limite_pago = res.data[i].fecha_limite_pago.substr(0,10);
          if(res.data[i].fecha_pago)
            res.data[i].fecha_pago = res.data[i].fecha_pago.substr(0,10);
        }
        $scope.compras = res.data;
      }, function(res){});
    });
    $scope.registrarPago = (function(co){$scope.compra = co});
    $('#guardarPagoCompra').click(function() {
      var req = {
        method: 'PUT',
        url: 'http://127.0.0.1:3000/api/pago-compra/' + $scope.compra._id + '/' + $scope.fecha_pago,
        headers: {'Content-Type': 'application/json'}
      };
      $http(req).then(function(res){
        $scope.compra.fecha_pago = $scope.fecha_pago;
        showMessage(res.data.message);
      }, function(res){});
    });
    $scope.editarCompra = (function(co) {
      co.editMode = true;
      $scope.compra = co;
    });
    $scope.actualizarCompra = (function() {
      var req = {
        method: 'PUT',
        url: 'http://127.0.0.1:3000/api/compras',
        headers: {'Content-Type': 'application/json'},
        data: $scope.compra
      };
      $http(req).then(function(res){
        $scope.compra.editMode = false;
        showMessage(res.data.message);
      }, function(res){});
    });
    $scope.eliminarCompra = (function(compra) {
      if(confirm('¿Seguro que quieres eliminar esta compra?')) {
        var id = $scope.compras.indexOf(compra);
        $scope.compras.splice(id, 1);
        var req = {
          method: 'DELETE',
          url: 'http://127.0.0.1:3000/api/compras/' + compra._id,
          headers: {'Content-Type': 'application/json'}
        };
        $http(req).then(function(res){
          showMessage(res.data.message);
        }, function(res){});
      }
    });
    $scope.seleccionarProducto = (function(pro) {
      if(pro.seleccionado)
        $scope.productos_seleccionados.push(pro);
      else {
        var index = $scope.productos_seleccionados.indexOf(pro);
        if(index > -1)
          $scope.productos_seleccionados.splice(index, 1);
      }
    });
    $scope.seleccionarProveedor = (function() {
      for (var i = 0; i < $scope.proveedores.length; i++) {
        if($scope.proveedores[i].nombre === $scope.proveedorSeleccionado)
          $scope.proveedor = $scope.proveedores[i];
      }
    });
    obtenerProveedores();
    obtenerProductos();
  } //Fin del controlador
]);
